package bes

import (
	"encoding/binary"
	"encoding/json"
	"github.com/boltdb/bolt"
	"sync"
	"time"
)

func New(db *bolt.DB, name string, shardId int64) *T {
	t := new(T)
	t.db = db
	t.namespace = []byte(name)
	t.db.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists(t.namespace)
		return err
	})

	t.shardId = shardId

	t.mu = &sync.Mutex{}
	t.cond = sync.NewCond(t.mu)

	return t
}

type LogMessage struct {
	ID   int64
	Data interface{}
}

func (lm *LogMessage) encode() []byte {
	b, _ := json.Marshal(lm)
	return b
}
func (lm *LogMessage) decode(v []byte) {
	json.Unmarshal(v, lm)
}

type T struct {
	db        *bolt.DB
	namespace []byte

	OnStoreFail func(LogMessage)

	mu   *sync.Mutex
	cond *sync.Cond

	shardId int64

	GetID func(int64) int64
}

func (t *T) Store(item interface{}) int64 {
	var msg LogMessage
	msg.ID = t.getId()
	msg.Data = item
	t.WriteRaw(msg)
	t.Notify()
	return msg.ID
}

func (t *T) Delete(olderThanUnixNano int64) {
	t.db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket(t.namespace)
		c := b.Cursor()
		for k, _ := c.Seek(itob(olderThanUnixNano)); k != nil; k, _ = c.Prev() {
			b.Delete(k)
		}
		return nil
	})
}

func (t *T) Play(fromTime, toTime int64, onMessage func(LogMessage)) {
	ch := make(chan LogMessage)
	go func() {
		start := fromTime
		for {
			lm := t.next(start)
			if lm.ID != -1 {
				start = lm.ID
			} else {
				break
			}
			if start > toTime {
				close(ch)
				return
			}
			ch <- lm
		}
		close(ch)
	}()
	go func() {
		for msg := range ch {
			onMessage(msg)
		}
	}()
}

func (t *T) PlayAndSubscribe(fromTime int64, onMessage func(LogMessage)) (unsub func()) {
	ch := make(chan LogMessage)
	go func() {
		start := fromTime
		for {
			lm := t.next(start)
			if lm.ID != -1 {
				start = lm.ID
			} else {
				t.mu.Lock()
				t.cond.Wait()
				t.mu.Unlock()
				continue
			}
			select {
			case _, ok := <-ch:
				if !ok {
					return
				}
			default:
			}

			select {
			case _, ok := <-ch:
				if !ok {
					return
				}
			case ch <- lm:
			}
		}
	}()
	go func() {
		for {
			msg, ok := <-ch
			if ok {
				onMessage(msg)
			}
		}
	}()
	return func() { close(ch) }
}

func (t *T) Notify() {
	t.mu.Lock()
	t.cond.Broadcast()
	t.mu.Unlock()
}

func (t *T) WriteRaw(lm LogMessage) {
	t.db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket(t.namespace)
		return b.Put(itob(lm.ID), lm.encode())
	})
}

func (t *T) First() LogMessage {
	return t.first()
}

func (t *T) Last() LogMessage {
	return t.last()
}

func (t *T) last() (lm LogMessage) {
	lm.ID = -1
	var v1 []byte
	t.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket(t.namespace)
		c := b.Cursor()
		_, v := c.Last()
		v1 = clone(v)
		return nil
	})
	if v1 != nil {
		lm.decode(v1)
	}
	return lm
}

func (t *T) first() (lm LogMessage) {
	lm.ID = -1
	var v1 []byte
	t.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket(t.namespace)
		c := b.Cursor()
		_, v := c.First()
		v1 = clone(v)
		return nil
	})
	if v1 != nil {
		lm.decode(v1)
	}
	return lm
}

func (t *T) Next(fromTime int64) LogMessage {
	return t.next(fromTime)
}

func (t *T) next(fromTime int64) LogMessage {
	var lm LogMessage
	lm.ID = -1
	var v1, v2 []byte
	t.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket(t.namespace)
		c := b.Cursor()
		_, tmp1 := c.Seek(itob(fromTime))
		_, tmp2 := c.Next()
		v1 = clone(tmp1)
		v2 = clone(tmp2)
		return nil
	})
	if v1 != nil {
		lm.decode(v1)
		if lm.ID > fromTime {
			return lm
		}
		if lm.ID == fromTime {
			lm.ID = -1
		}
	}
	if v2 != nil {
		lm.decode(v2)
	}
	return lm
}

func (t *T) getId() int64 {
	if t.GetID == nil {
		return getId(t.shardId)
	}
	return t.GetID(t.shardId)
}

func getId(shardId int64) int64 {
	id := time.Now().UnixNano()
	id = id - (id % 100) + shardId
	return id
}

func itob(v int64) []byte {
	b := make([]byte, 8)
	binary.BigEndian.PutUint64(b, uint64(v))
	return b
}

func clone(b []byte) []byte {
	r := make([]byte, len(b))
	copy(r, b)
	return r
}
